#!/bin/bash

# Initialize default values
PROJECT=""

# Parse command-line options
while getopts "d:a:p:" flag; do
  case "${flag}" in
    d) DIR="${OPTARG}";;        # -d for directory
    a) ARTIFACT_ID="${OPTARG}";; # -a for artifact_id
    p) PROJECT="/${OPTARG}";;    # -p for project (optional)
    *) 
      echo "Usage: $0 -d <directory> -a <artifact_id> [-p <project>]"
      exit 1;;
  esac
done

# Ensure required arguments are provided
if [ -z "$DIR" ] || [ -z "$ARTIFACT_ID" ]; then
    echo "Usage: $0 -d <directory> -a <artifact_id> [-p <project>]"
    exit 1
fi

# Adjust the project variable if not provided
[ -n "$PROJECT" ] || PROJECT=""

case $DIR in
    vertical-apps/)
        helm package "$DIR$ARTIFACT_ID" -d "$DIR"
        # Find .tgz files
        files=($DIR*.tgz)
        # Count the number of files found
        file_count=${#files[@]}
        if [ $file_count -eq 1 ]; then
            echo "Exactly one .tgz file found: ${files[0]}"
            helm push "${files[0]}" OCI://registry.nephele-hdar.netmode.ece.ntua.gr$PROJECT
        else 
            echo "No .tgz files found or more than one."
            echo "Not found .tgz file, check logs"
            exit 1
        fi
        ;;
    *)
        hdarctl package tar "$DIR$ARTIFACT_ID" -d "$DIR"
        # Find .tar.gz files
        files=($DIR*.tar.gz)
        # Count the number of files found
        file_count=${#files[@]}
        if [ $file_count -eq 1 ]; then
            echo "Exactly one .tar.gz file found: ${files[0]}"
            hdarctl push "${files[0]}" registry.nephele-hdar.netmode.ece.ntua.gr$PROJECT
        else 
            echo "No .tar.gz files found or more than one."
            echo "Not found .tar.gz file, check logs"
            exit 1
        fi
        ;;
esac
